from .models import Messenger, TestCode, Conversation, Message
from rest_framework import serializers
from django.contrib.auth.models import User


class MessengerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Messenger
        fields = ('mobile_number',)

    def get_or_create(self):
        mobile_number = self.validated_data['mobile_number']
        try:
            messenger = Messenger.objects.get(mobile_number=mobile_number)
        except Messenger.DoesNotExist:
            user, created = User.objects.get_or_create(username=str(mobile_number))
            messenger = Messenger.objects.create(user=user, mobile_number=mobile_number)
        return messenger


class TestCodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestCode
        fields = ('mobile_number', 'code_number')


class ConversationListSerializer(serializers.ModelSerializer):
    link = serializers.HyperlinkedIdentityField(view_name='conv-detail', read_only=True)
    other = MessengerSerializer(
        read_only=True,
     )

    class Meta:
        model = Conversation
        fields = ('other', 'link')


class MessageSerializer(serializers.ModelSerializer):
    owner = MessengerSerializer(
        read_only=True,
     )

    class Meta:
        model = Message
        fields = ('owner', 'text')


class ConversationSerializer(serializers.ModelSerializer):
    other = MessengerSerializer(
        read_only=True,
     )
    messages = MessageSerializer(many=True, read_only=True)

    class Meta:
        model = Conversation
        fields = ('other', 'messages')
