from django.contrib.auth.models import User
from rest_framework import status, permissions, generics
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Conversation
from .models import TestCode
from .serializers import MessengerSerializer, TestCodeSerializer, ConversationListSerializer, ConversationSerializer


class Register(APIView):
    def post(self, request, format=None):
        serializer = MessengerSerializer(data=request.data)
        if serializer.is_valid():
            messenger = serializer.get_or_create()
            code, created = TestCode.objects.get_or_create(
                mobile_number=messenger.mobile_number,
                defaults={'code_number': TestCode.generate_code()}
            )
            print('Send code via SMS:', code.code_number)
            return Response({'code': 'SMS_code_sent'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class Verification(APIView):
    def post(self, request, format=None):
        serializer = TestCodeSerializer(data=request.data)
        if serializer.is_valid():
            try:
                TestCode.objects.get(**serializer.data)
            except TestCode.DoesNotExist:
                return Response({'error': 'invalid_validation_code'}, status=status.HTTP_400_BAD_REQUEST)
            user = User.objects.get(my_messenger__mobile_number=serializer.data['mobile_number'])
            return Response({'token': str(user.auth_token)}, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ConversationList(generics.ListAPIView):
    queryset = Conversation.objects.all()
    serializer_class = ConversationListSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        qs = super().get_queryset()
        qs.filter(messenger__user=self.request.user)
        return qs


class ConversationDetail(generics.RetrieveAPIView):
    serializer_class = ConversationSerializer
    queryset = Conversation.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        qs = super().get_queryset()
        qs.filter(messenger__user=self.request.user)
        return qs
