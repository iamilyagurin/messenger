from django.db import models
from django.contrib.auth.models import User
from django.conf import settings


class Messenger(models.Model):
    user = models.OneToOneField(User, related_name='my_messenger')
    mobile_number = models.PositiveIntegerField()


class Message(models.Model):
    owner = models.ForeignKey(Messenger)
    text = models.TextField()


class Conversation(models.Model):
    messenger = models.ForeignKey(Messenger, related_name='my_conversation')
    other = models.ForeignKey(Messenger)
    messages = models.ManyToManyField(Message)

    class Meta:
        unique_together = ('messenger', 'other')


class TestCode(models.Model):
    mobile_number = models.PositiveIntegerField()
    code_number = models.PositiveIntegerField()

    @staticmethod
    def generate_code():
        return settings.TEST_CODE
