
from django.conf.urls import url, include
from rest_framework import routers
from core import views

router = routers.DefaultRouter()

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^register$', views.Register.as_view()),
    url(r'^verification$', views.Verification.as_view()),
    url(r'^conversations$', views.ConversationList.as_view()),
    url(r'^conversations/(?P<pk>[0-9]+)/$', views.ConversationDetail.as_view(), name='conv-detail'),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
